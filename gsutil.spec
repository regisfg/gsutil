Summary: A utility to save, restore and reboot Grandstream Budgetone phones
Name: gsutil
Version: 3.1
Release: 1
Group: Applications/Internet
Source: http://www.pkts.ca/%{name}-%{version}.tar.gz
License: GPL
Vendor: Charles Howes <gsutil@ch.pkts.ca>
URL: http://www.pkts.ca/gsutil.shtml
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
Requires: perl
Requires: perl-libwww-perl
Provides: gsutil

%description
A utility to dump and restore the configuration of the Grandstream
Budgetone VOIP telephone, up to and including version 1.0.8.23 of the
firmware on the BudgeTone and 1.1.0.16 on the GXP2000.

%prep
%setup -q

%build

%install
rm -rf $RPM_BUILD_ROOT
umask 022
mkdir -p $RPM_BUILD_ROOT%{_bindir}
cp %{_builddir}/%{name}-%{version}/gsutil $RPM_BUILD_ROOT%{_bindir}/gsutil

%clean
rm -r $RPM_BUILD_ROOT

%files 
%defattr(0644,root,root,0755)
%attr(0755,root,root) %{_bindir}/gsutil
%doc README Changelog

%changelog
* Thu Aug 13 2009 Charles Howes <gsutil@ch.pkts.ca> 3.1-1
- Add busy-detect code and -w option
  If phone is busy, add it to the end of the list, with a 5-second pause

* Thu Jul 27 2006 Charles Howes <gsutil@ch.pkts.ca> 3.0-1
- Rewrite!
  It was discovered that
    a) gsutil no longer worked on BT101's, only GXP-2000's
    b) my programming style has gotten better since 2.x
  So, it's rewritten, making it more organized, smaller, and smarter.
  I never want to see 'if ($version eq "xxx")' again!
  Note: I *only* have a BT101, so it would seem I should have actually
  *tested* the previous releases.  In fact, I was accepting patches
  from GXP-2000 people and the BT101 was gathering dust on a shelf.
  Fixed.

* Wed Jul 12 2006 Charles Howes <gsutil@ch.pkts.ca> 2.5-1
- Changes from Ken Yap, Hans Martin, Anthony McCarthy, Jerome
  Warnier, Tom Mealey, Marcus Uy, Shane Steinbeck, Enrique Zanardi,
  Frank Scholz, and Marcel van der Boom, to deal with the new options
  provided by new firmware (GXP-2000/1.1.0.16).  I should have
  updated sooner; the various patches had different suggestions
  for how to spell the options, and so I had to pick the ones
  that seemed best.  Hopefully that doesn't cause anyone too
  much work.  Firmware is at  http://grandstream.com/y-firmware.htm
- Also fixed a crash when a page isn't found on the phone.

* Sat Feb  4 2006 Charles Howes <gsutil@ch.pkts.ca> 2.4-1
- Changes from Lukas Ziaja to support GXP-2000 firmware 1.0.2.3
- http://grandstream.com/BETATEST/GXP2000/Release_1.0.2.3_GXP2000.zip 

* Thu Jan 26 2006 Charles Howes <gsutil@ch.pkts.ca> 2.3-1
- Changes from Alan Ferrency to support GXP-2000 firmware b1.0.1.2/P1.0.1.9

* Sat Sep 10 2005 Charles Howes <gsutil@ch.pkts.ca> 2.2-1
- Minor tweak from Frank Scholz to support ATA-286 fully

* Thu Aug 18 2005 Charles Howes <gsutil@ch.pkts.ca> 2.1-1
- Actually check to see if the password was correct... Doh!

* Sat Jul 30 2005 Charles Howes <gsutil@ch.pkts.ca> 2.0-1
- Made it work for 1.0.6.x firmware phones
- Possibly broke the 1.0.5.x firmware code, but maybe not; untested.

* Wed Jul 27 2005 Charles Howes <gsutil@ch.pkts.ca> 1.2-1
- added version check, print
- added GS format

* Mon Jul 12 2005 Charles Howes <gsutil@ch.pkts.ca> 1.1-1
- check hostname before processing

* Sat Jul  9 2005 Charles Howes <gsutil@ch.pkts.ca> 1.0-1
- initial build
