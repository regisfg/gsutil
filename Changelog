Here is a history of changes to gsutil.
3.1       Add busy-detect code and -w option
	  If a phone is busy, add it to the end of the list, with a 5-second pause
	  added if no pauses remain on the list.

3.0       Rewrite!
          It was discovered that
	    a) gsutil no longer worked on BT101's, only GXP-2000's
	    b) my programming style has gotten better since 2.x
	  So, it's rewritten, making it more organized, smaller, and smarter.
	  I never want to see 'if ($version eq "xxx")' again!
	  Note: I *only* have a BT101, so it would seem I should have actually
	  *tested* the previous releases.  In fact, I was accepting patches
	  from GXP-2000 people and the BT101 was gathering dust on a shelf.
	  Fixed.

2.5       Changes from Ken Yap, Hans Martin, Anthony McCarthy, Jerome
          Warnier, Tom Mealey, Marcus Uy, Shane Steinbeck, Enrique Zanardi,
          Frank Scholz, and Marcel van der Boom, to deal with the new options
	  provided by new firmware (GXP-2000/1.1.0.16).  I should have
	  updated sooner; the various patches had different suggestions
	  for how to spell the options, and so I had to pick the ones
	  that seemed best.  Hopefully that doesn't cause anyone too
	  much work.  Firmware is at  http://grandstream.com/y-firmware.htm
	  Also fixed a crash when a page isn't found on the phone.

2.4       Changes from Lukas Ziaja to support GXP-2000 firmware 1.0.2.3
          http://grandstream.com/BETATEST/GXP2000/Release_1.0.2.3_GXP2000.zip 

2.3     - Add changes by Alan Ferrency for the GXP-2000, firmware
	  b1.0.1.2/P1.0.1.9

2.2     - Add a few more configuration options for the GS ATA-286
	  (P86,P200,P205,P206,P228).  Patch provided by Frank Scholz.
	  Thank you Frank!

2.1     - Check to see if the password was accepted.
	- Check all results more thoroughly.

2.0     - Work with firmware 1.0.6.x
	- I need to test this with 1.0.5.11 still; I hope I didn't break it,
	  but I accidentally upgraded my second phone and can't test it
	  now.

1.2	- Display version numbers of firmware (-e)
	- Refuse to run if can't find firmware release, because things
	  have changed radically from version 1.0.5.11 to 1.0.6.7.
	- Optionally read and write config files in Grandstream ascii
	  format (-o)

1.1	- Check to make sure gethostbyname finds phones before
	  proceeding (give a better error message)

1.0	- Initial release
